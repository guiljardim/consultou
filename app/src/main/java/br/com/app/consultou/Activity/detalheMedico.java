package br.com.app.consultou.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;

import Model.Medico;
import br.com.app.consultou.R;

public class detalheMedico extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private TextView nome, crm, telefone, rua, bairro, municipioEstado, cep, especialidade, detalhes;

    private static final String TAG = detalheMedico.class.getSimpleName();
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private String medicoId;
    private Medico medico;
    TextView semHorario;

    private ArrayList<String> horarios = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_medico);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("medicos");
        Intent intent = getIntent();
        medicoId = intent.getStringExtra("medicoId");

        nome = findViewById(R.id.nome);
        crm = findViewById(R.id.crm);
        telefone = findViewById(R.id.telefone);
        rua = findViewById(R.id.rua);
        bairro = findViewById(R.id.bairro);
        municipioEstado = findViewById(R.id.municipioEstado);
        cep = findViewById(R.id.cep);
        especialidade = findViewById(R.id.especialidade);
        detalhes = findViewById(R.id.detalhes);


        ValueEventListener readUser = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {
                    Medico medicoIf = ds.getValue(Medico.class);
                    if (medicoIf.getMedicoId().equals(medicoId)) {
                        medico = medicoIf;
                        horarios = medico.getHorarios();
                    }
                }

                nome.setText(medico.getNome());
                crm.setText(medico.getCrm());
                telefone.setText(medico.getTelefone());
                rua.setText(medico.getRua());
                bairro.setText(medico.getBairro());
                String munEst = medico.getMunicipio() + ", " + medico.retornarEstadoAbreviado();
                municipioEstado.setText(munEst);
                cep.setText(medico.getCep());
                especialidade.setText(medico.getEspecialidade());
                detalhes.setText(medico.getDetalhes());

                ArrayList<String> horariosDisponiveis = new ArrayList<>();

                if(medico.getHorarios() != null){
                for (String horDisponivel :
                        medico.getHorarios()) {
                    String[] partes = horDisponivel.split(",");
                    if (partes[2].equals("true")) {
                        horariosDisponiveis.add(partes[0] + " - " + partes[1]);
                    }
                }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(detalheMedico.this, R.layout.activity_cell_horario, horariosDisponiveis);
                ListView listView = findViewById(R.id.listaDeHorarios);
                listView.setOnItemClickListener(detalheMedico.this);
                listView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, databaseError.toException());
            }
        };

        mFirebaseDatabase.addValueEventListener(readUser);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String horarioClicado = horarios.remove(i);
        final String[] partes = horarioClicado.split(",");
        semHorario = findViewById(R.id.semHorario);

        medico.trocarDisponibilidade(partes[0], partes[1]);
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mFirebaseDatabase.child(medico.getMedicoId()).setValue(medico);
                int index = 0;
                if(horarios != null)
                for (String hora :
                        horarios) {
                    if (hora.equals("Sem horarios disponiveis")) {
                        horarios.remove(index);
                    }
                    index = index + 1;
                }
                if (horarios == null) {
                    semHorario.setText("Sem Horário Disponível");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mFirebaseDatabase.addValueEventListener(postListener);
        Toast.makeText(detalheMedico.this, "Horario agendado", Toast.LENGTH_LONG).show();

    }
}
