package br.com.app.consultou.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.content.Context;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Adapter.MedicosAdapter;
import Adapter.MyDividerItemDecoration;
import Adapter.RecyclerTouchListener;
import Model.DiasDaSemana;
import Model.Endereco;
import Model.Medico;
import Repository.MedicoRepository;
import Repository.OnGetDataListener;
import br.com.app.consultou.R;

public class MainActivity extends AppCompatActivity {

    private AdView mAdView;

    private List<Medico> medicoLista = new ArrayList<>();
    private RecyclerView recyclerView;
    private MedicosAdapter mAdapter;

    private static final String TAG = MainActivity.class.getSimpleName();
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    String medicoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("medicos");
        mAdapter = new MedicosAdapter(medicoLista);
        prepareMedicoData(mFirebaseDatabase);

        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, "ca-app-pub-2413868023665754~5823315392");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        criarMedico();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mFirebaseInstance.getReference("app_title").setValue("consultou");

        mFirebaseInstance.getReference("app_title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e(TAG, "App title updated");

                String appTitle = dataSnapshot.getValue(String.class);
                Log.e(TAG, appTitle);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Failed to read app title value", databaseError.toException());
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Medico medico = medicoLista.get(position);
                Intent intent = new Intent(MainActivity.this, detalheMedico.class);
                intent.putExtra("medicoId", medico.getMedicoId());
                startActivity(intent);
                Toast.makeText(getApplicationContext(), medico.getNome() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    private void criarMedico() {
        medicoId = mFirebaseDatabase.push().getKey();
        String medicoId2 = mFirebaseDatabase.push().getKey();
        String medicoId3 = mFirebaseDatabase.push().getKey();
        String medicoId4 = mFirebaseDatabase.push().getKey();
        String medicoId5 = mFirebaseDatabase.push().getKey();
        String medicoId6 = mFirebaseDatabase.push().getKey();
        String medicoId7 = mFirebaseDatabase.push().getKey();
        String medicoId8 = mFirebaseDatabase.push().getKey();
        String medicoId9 = mFirebaseDatabase.push().getKey();
        String medicoId10 = mFirebaseDatabase.push().getKey();
        String medicoId11 = mFirebaseDatabase.push().getKey();
        String medicoId12 = mFirebaseDatabase.push().getKey();
        String medicoId13 = mFirebaseDatabase.push().getKey();


        Medico medico = new Medico(medicoId, "Guilherme", "69", "242", "Rua Conde de Bonfim", "Tijuca", "Rio de Janeiro", "Rio de Janeiro", "4525", "Ginecologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico2 = new Medico(medicoId2, "Paulo", "691", "24212333", "Rua Paulo Silva Araujo", "Meier", "Rio de Janeiro", "Rio de Janeiro", "4525", "Urologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico3 = new Medico(medicoId3, "Pedro", "693", "24212455", "Rua Belford Roxo", "Copacabana", "Rio de Janeiro", "Rio de Janeiro", "4525", "Otorrinolaringologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico4 = new Medico(medicoId4, "Marcos", "693", "87654433", "Rua 28 de Setembro", "Vila Isabel", "Rio de Janeiro", "Rio de Janeiro", "4525", "Alergista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico5 = new Medico(medicoId5, "Leticia", "694", "24567432", "Rua General Renato Paquet", "Barra da Tijuca", "Rio de Janeiro", "Rio de Janeiro", "4525", "Oncologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico6 = new Medico(medicoId6, "Isabela", "695", "12345678", "Rua São Jose", "Centro", "Rio de Janeiro", "Rio de Janeiro", "4525", "Cardiologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico7 = new Medico(medicoId7, "Davi", "696", "43221212", "Rua Alberto Leite", "Meier", "Rio de Janeiro", "Rio de Janeiro", "4525", "Clinico Geral","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico8 = new Medico(medicoId8, "Lucas", "697", "22222222", "Rua Macena", "Maracanã", "Rio de Janeiro", "Rio de Janeiro", "4525", "Dermatologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico9 = new Medico(medicoId9, "Marina", "698", "22222222", "Rua Paris", "Bonsucesso", "Rio de Janeiro", "Rio de Janeiro", "4525", "Gastrologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico10 = new Medico(medicoId10, "Fernanda", "699", "22222222", "Rua Fernandes Gusmão", "Irajá", "Rio de Janeiro", "Rio de Janeiro", "4525", "Geriatra","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico11 = new Medico(medicoId11, "Luciana", "6910", "22222222", "Rua Mariz e Barros", "Tijuca", "Rio de Janeiro", "Rio de Janeiro", "4525", "Neurologista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico12 = new Medico(medicoId12, "Marcio", "6911", "22222222", "Rua Dias da Cruz", "Meier", "Rio de Janeiro", "Rio de Janeiro", "4525", "Nutricionista","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        Medico medico13 = new Medico(medicoId13, "Paula", "6912", "222222222", "Rua Ana Leonidia", "Engenho de Dentro", "Rio de Janeiro", "Rio de Janeiro", "4525", "Pediatra","Desta maneira, a execução dos pontos do programa nos obriga à análise das formas de ação. Evidentemente, o acompanhamento das preferências de consumo pode nos levar a considerar a reestruturação dos métodos utilizados na avaliação de resultados.");
        medico.setHorarios("Terca", "09:00", false);
        medico2.setHorarios("Terca", "10:00", false);
        medico3.setHorarios("Terca", "11:00", false);
        medico4.setHorarios("Terca", "12:00", true);
        medico5.setHorarios("Terca", "13:00", true);
        medico6.setHorarios("Terca", "14:00", false);
        medico7.setHorarios("Terca", "15:00", false);
        medico8.setHorarios("Terca", "16:00", true);
        medico9.setHorarios("Terca", "17:00", true);
        medico10.setHorarios("Terca", "18:00", false);
        medico11.setHorarios("Terca", "09:00", false);
        medico12.setHorarios("Terca", "12:00", true);
        medico13.setHorarios("Terca", "13:00", true);
        medico2.setHorarios("Terca", "14:00", false);
        medico3.setHorarios("Terca", "15:00", false);
        medico4.setHorarios("Terca", "16:00", true);
        medico5.setHorarios("Terca", "19:00", true);
        medico6.setHorarios("Terca", "11:00", false);
        medico7.setHorarios("Terca", "12:00", false);
        medico8.setHorarios("Terca", "14:00", true);
        medico9.setHorarios("Terca", "15:00", true);
        medico10.setHorarios("Terca", "11:00", false);
        medico11.setHorarios("Terca", "12:00", false);
        medico12.setHorarios("Terca", "15:00", true);
        medico13.setHorarios("Terca", "16:00", true);
        mFirebaseDatabase.child(medicoId).setValue(medico);
        mFirebaseDatabase.child(medicoId2).setValue(medico2);
        mFirebaseDatabase.child(medicoId3).setValue(medico3);
        mFirebaseDatabase.child(medicoId4).setValue(medico4);
        mFirebaseDatabase.child(medicoId5).setValue(medico5);
        mFirebaseDatabase.child(medicoId6).setValue(medico6);
        mFirebaseDatabase.child(medicoId7).setValue(medico7);
        mFirebaseDatabase.child(medicoId8).setValue(medico8);
        mFirebaseDatabase.child(medicoId9).setValue(medico9);
        mFirebaseDatabase.child(medicoId10).setValue(medico10);
        mFirebaseDatabase.child(medicoId11).setValue(medico11);
        mFirebaseDatabase.child(medicoId12).setValue(medico12);
        mFirebaseDatabase.child(medicoId13).setValue(medico13);

    }

    private void prepareMedicoData(DatabaseReference mFirebaseDatabase) {

        MedicoRepository medicoRepository = new MedicoRepository();
        medicoRepository.mReadDataOnce(mFirebaseDatabase,new OnGetDataListener() {
            @Override
            public void onStart() {
                //DO SOME THING WHEN START GET DATA HERE
            }

            @Override
            public void onSuccess(DataSnapshot data) {
                for (DataSnapshot ds : data.getChildren()) {
                    String medicoId = ds.getKey();
                    String nome = (String) ds.child("nome").getValue();
                    String telefone = (String) ds.child("telefone").getValue();
                    String especialidade = (String) ds.child("especialidade").getValue();
                    Medico medico = new Medico(nome, telefone, especialidade);
                    medico.setMedicoId(medicoId);
                    medicoLista.add(medico);

                    Log.e("MedicoRepository", ds.child("nome").getValue().toString());
                    mAdapter.notifyDataSetChanged();
                }
             }

            @Override
            public void onFailed(DatabaseError databaseError) {
                //DO SOME THING WHEN GET DATA FAILED HERE
            }
        });


    }

    public void irParaLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    //Encerrar sessão//
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            irParaLoginActivity();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}