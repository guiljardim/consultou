package br.com.app.consultou.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import br.com.app.consultou.R;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;


public class RecuperarSenha extends AppCompatActivity {

    EditText email;
    Button btnRecuperarSenha;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_senha);

        email = (EditText) findViewById(R.id.emailRecuperarSenha);
        btnRecuperarSenha = (Button) findViewById(R.id.btnRecuperarSenhaLink);
        firebaseAuth = FirebaseAuth.getInstance();

        btnRecuperarSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enderecoEmail = email.getText().toString();

                if(TextUtils.isEmpty(enderecoEmail)){
                    Toast.makeText(getApplicationContext(),"Preenche o email",Toast.LENGTH_SHORT).show();
                    return;
                }
                firebaseAuth.sendPasswordResetEmail(enderecoEmail)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(getApplicationContext(),"Um link para redefinição de senha foi enviado para o seu e-mail",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"Ocorreu um erro ao enviar o e-mail",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }});
    }
}
