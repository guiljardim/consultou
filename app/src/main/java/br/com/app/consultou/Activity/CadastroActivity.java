package br.com.app.consultou.Activity;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import Model.Usuario;
import br.com.app.consultou.R;

public class CadastroActivity extends AppCompatActivity implements DatabaseReference.CompletionListener, View.OnClickListener{

    private EditText nome, email, password;
    private Button registerButton;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    private Usuario usuario;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);


        nome = (EditText) findViewById(R.id.cadastroNome);
        email = (EditText) findViewById(R.id.cadastroEmail);
        password = (EditText) findViewById(R.id.cadastroSenha);
        registerButton = (Button) findViewById(R.id.btnCadastrar);

        registerButton.setOnClickListener(this);

        firebaseAuth = FirebaseAuth.getInstance();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

                if (firebaseUser == null || usuario.getId() != null) {
                    return;
                }
                usuario.setId(firebaseUser.getUid());
                usuario.saveDB(CadastroActivity.this);
            }
        };

        inicializarViews();

        registerButton = (Button) findViewById(R.id.btnCadastrar);
        registerButton.setOnClickListener(this);
    }

    private void inicializarViews() {

        nome = (EditText) findViewById(R.id.cadastroNome);
        email = (EditText) findViewById(R.id.cadastroEmail);
        password = (EditText) findViewById(R.id.cadastroSenha);
        progressBar = (ProgressBar) findViewById(R.id.sign_up_progress);
    }

    private void showToast(String message) {

        Toast.makeText(this,
                message,
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onClick(View v) {
        inicializarUsuario();

        String NOME = nome.getText().toString();
        String EMAIL = email.getText().toString();
        String SENHA = password.getText().toString();

        boolean ok = true;

        if (NOME.isEmpty()) {
            nome.setError("O campo nome não pode ser vazio");
            ok = false;
        }

        if (EMAIL.isEmpty()) {
            email.setError("O campo email não pode ser vazio");
            ok = false;
        }

        if (SENHA.isEmpty()) {
            password.setError("Por favor, informe uma senha!");
            ok = false;
        }

        if (SENHA.length() < 6){
            Toast.makeText(getApplicationContext(), "Senha muito curta, minimo 6 characteres!", Toast.LENGTH_SHORT).show();
            ok = false;
        }

        if (ok) {
            registerButton.setEnabled(false);
            progressBar.setFocusable(true);

            openProgressBar();
            salvaUsuario();
        } else {
            closeProgressBar();
        }
    }

    private void inicializarUsuario() {

        usuario = new Usuario();
        usuario.setName(nome.getText().toString());
        usuario.setEmail(email.getText().toString());
        usuario.setPassword(password.getText().toString());
    }

    private void salvaUsuario() {

        firebaseAuth.createUserWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getPassword()
        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (!task.isSuccessful()) {
                    closeProgressBar();
                }
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showSnackbar(e.getMessage());
                registerButton.setEnabled(true);
            }
        });
    }

    private void showSnackbar(String message) {

        Snackbar.make(progressBar,
                message,
                Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void closeProgressBar() {

        progressBar.setVisibility( View.GONE );
    }

    private void openProgressBar() {

        progressBar.setVisibility( View.VISIBLE );
    }

    @Override
    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
        firebaseAuth.signOut();

        showToast("Usuário criado com sucesso!");
        closeProgressBar();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthStateListener != null) {
            firebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
    }
}