package Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import Model.Medico;
import br.com.app.consultou.R;

public class MedicosAdapter extends RecyclerView.Adapter<MedicosAdapter.MyViewHolder>{

    private List<Medico> medicoLista;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nome, telefone, especialidade;

        public MyViewHolder(View view) {
            super(view);
            nome = (TextView) view.findViewById(R.id.nome);
            telefone = (TextView) view.findViewById(R.id.telefone);
            especialidade = (TextView) view.findViewById(R.id.especialidade);
        }
    }

    public MedicosAdapter(List<Medico> medicoLista) {
        this.medicoLista = medicoLista;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medico_lista_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Medico medico = medicoLista.get(position);
        holder.nome.setText(medico.getNome());
        holder.telefone.setText(medico.getTelefone());
        holder.especialidade.setText(medico.getEspecialidade());
    }

    @Override
    public int getItemCount() {
        return medicoLista.size();
    }
}
