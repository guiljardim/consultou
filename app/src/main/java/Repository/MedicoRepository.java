package Repository;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import Model.Medico;

public class MedicoRepository {


    public void mReadDataOnce(DatabaseReference mFirebaseDatabase, final OnGetDataListener listener) {
        listener.onStart();
        mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
            }
        });
    }

    ArrayList<Medico> medicoArrayList = new ArrayList<>();

    Boolean status = false;
    public ArrayList<Medico> ListarMedicos(DatabaseReference mFirebaseDatabase) {


        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String medicoId = ds.getKey();
                    String nome = (String) ds.child("nome").getValue();
                    String telefone = (String) ds.child("telefone").getValue();
                    String especialidade = (String) ds.child("especialidade").getValue();
                    Medico medico = new Medico(nome, telefone, especialidade);
                    medico.setMedicoId(medicoId);
                    medicoArrayList.add(medico);

                    Log.e("MedicoRepository", ds.child("nome").getValue().toString());
                    status = true;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("MedicoRepository", "Não achou dados", databaseError.toException());
            }

        };

        mFirebaseDatabase.addListenerForSingleValueEvent(eventListener);
        while(status) {
            return medicoArrayList;
        }
        return medicoArrayList;
    }
}
