package Model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

@IgnoreExtraProperties
public class Medico {
    private String medicoId;
    private String nome;
    private String crm;
    private String telefone;
    private String rua;
    private String bairro;
    private String municipio;
    private String estado;
    private String cep;
    private String especialidade;
    private String detalhes;
    private ArrayList<String> horarios;

    public Medico() {
    }

    public Medico(String nome, String crm, String telefone) {
        this.nome = nome;
        this.crm = crm;
        this.telefone = telefone;
    }

    public Medico(String medicoId, String nome, String crm, String telefone, String rua, String bairro, String municipio, String estado, String cep, String especialidade, String detalhes) {
        this.medicoId = medicoId;
        this.nome = nome;
        this.crm = crm;
        this.telefone = telefone;
        this.rua = rua;
        this.bairro = bairro;
        this.municipio = municipio;
        this.estado = estado;
        this.cep = cep;
        this.especialidade = especialidade;
        this.detalhes = detalhes;
        horarios = new ArrayList<>();
    }

    public String getMedicoId() {
        return medicoId;
    }

    public void setMedicoId(String medicoId) {
        this.medicoId = medicoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public void setHorarios(String diaDaSemana, String hora, boolean disponibilidade) {
        String horariosParaSet = diaDaSemana + "," + hora + "," + disponibilidade;
        this.horarios.add(horariosParaSet);
    }

    public boolean disponivel(String diaDaSemana, String hora) {
        boolean retorno = false;

        for (String dia :
                this.horarios) {
            String[] partes = dia.split(",");
            if (partes[0].equals(diaDaSemana) && partes[1].equals(hora) && partes[2].equals("true")) {
                retorno = true;
            }
        }

        return retorno;
    }

    public void trocarDisponibilidade(String diaDaSemana, String hora) {
        int index = 0;

        for (String dia :
                this.horarios) {
            String[] partes = dia.split(",");
            if (partes[0].equals(diaDaSemana) && partes[1].equals(hora) && partes[2].equals("true")) {
                this.horarios.set(index, diaDaSemana + "," + hora + "," + false);
            }
            if (partes[0].equals(diaDaSemana) && partes[1].equals(hora) && partes[2].equals("false")) {
                this.horarios.set(index, diaDaSemana + "," + hora + "," + true);
            }
            index = index + 1;
        }

    }

    public ArrayList<String> getHorarios() {
        return horarios;
    }

    public String retornarEstadoAbreviado() {
        if (this.estado.equals("Acre")) {
            return estadosAbreviados.AC.toString();
        } else if (this.estado.equals("Alagoas")) {
            return estadosAbreviados.AL.toString();
        } else if (this.estado.equals("Amapá")) {
            return estadosAbreviados.AP.toString();
        } else if (this.estado.equals("Amazonas")) {
            return estadosAbreviados.AM.toString();
        } else if (this.estado.equals("Bahia")) {
            return estadosAbreviados.BA.toString();
        } else if (this.estado.equals("Ceará")) {
            return estadosAbreviados.CE.toString();
        } else if (this.estado.equals("Distrito Federal")) {
            return estadosAbreviados.DF.toString();
        } else if (this.estado.equals("Espírito Santo")) {
            return estadosAbreviados.ES.toString();
        } else if (this.estado.equals("Maranhão")) {
            return estadosAbreviados.GO.toString();
        } else if (this.estado.equals("Mato Grosso")) {
            return estadosAbreviados.MA.toString();
        } else if (this.estado.equals("Mato Grosso do Sul")) {
            return estadosAbreviados.MT.toString();
        } else if (this.estado.equals("Minas Gerais")) {
            return estadosAbreviados.MS.toString();
        } else if (this.estado.equals("Pará")) {
            return estadosAbreviados.MG.toString();
        } else if (this.estado.equals("Paraíba")) {
            return estadosAbreviados.PA.toString();
        } else if (this.estado.equals("Paraná")) {
            return estadosAbreviados.PB.toString();
        } else if (this.estado.equals("Pernambuco")) {
            return estadosAbreviados.PR.toString();
        } else if (this.estado.equals("Piauí")) {
            return estadosAbreviados.PE.toString();
        } else if (this.estado.equals("Goiás")) {
            return estadosAbreviados.PI.toString();
        } else if (this.estado.equals("Roraima")) {
            return estadosAbreviados.RR.toString();
        } else if (this.estado.equals("Rondônia")) {
            return estadosAbreviados.RO.toString();
        } else if (this.estado.equals("Rio de Janeiro")) {
            return estadosAbreviados.RJ.toString();
        } else if (this.estado.equals("Rio Grande do Norte")) {
            return estadosAbreviados.RN.toString();
        } else if (this.estado.equals("Rio Grande do Sul")) {
            return estadosAbreviados.RS.toString();
        } else if (this.estado.equals("Santa Catarina")) {
            return estadosAbreviados.SC.toString();
        } else if (this.estado.equals("São Paulo")) {
            return estadosAbreviados.SP.toString();
        } else if (this.estado.equals("Sergipe")) {
            return estadosAbreviados.SE.toString();
        } else if (this.estado.equals("Tocantins")) {
            return estadosAbreviados.TO.toString();
        } else {
            return "XX";
        }

    }

    private enum estadosAbreviados {
        AC, AL, AP, AM, BA, CE, DF, ES, GO, MA, MT, MS, MG, PA, PB, PR, PE, PI, RR, RO, RJ, RN, RS, SC, SP, SE, TO;
    }
}
