package Model;

import java.util.ArrayList;

public class DiasDaSemana {

    private diasDaSemana dia;
    private ArrayList<horarios> horario;

    public DiasDaSemana(diasDaSemana dia, ArrayList<horarios> horario) {
        this.dia = dia;
        this.horario = horario;
    }

    public diasDaSemana getDia() {
        return dia;
    }

    public ArrayList<horarios> getHorario() {
        return horario;
    }

    public enum diasDaSemana {
        Segunda, Terca, Quarta, Quinta, Sexta;

    }

    public enum horarios {
        Sete, Oito, Nove, Dez, Onze, Doze, Treze, Quatorze, Quinze, Dezesseis, Dezesete, Dezoito
    }
}
