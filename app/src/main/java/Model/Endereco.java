package Model;

public class Endereco {
    private String endereco;
    private String bairro;
    private String municipio;
    private String estado;
    private String cep;

    public Endereco() {
    }

    public Endereco(String endereco, String bairro, String estado, String cep, String municipio) {
        this.endereco = endereco;
        this.bairro = bairro;
        this.estado = estado;
        this.cep = cep;
        this.municipio = municipio;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public String getEstado() {
        return estado;
    }

    public String getCep() {
        return cep;
    }

    public String retornarEstadoAbreviado() {
        if (this.estado.equals("Acre")) {
            return estadosAbreviados.AC.toString();
        } else if (this.estado.equals("Alagoas")) {
            return estadosAbreviados.AL.toString();
        } else if (this.estado.equals("Amapá")) {
            return estadosAbreviados.AP.toString();
        } else if (this.estado.equals("Amazonas")) {
            return estadosAbreviados.AM.toString();
        } else if (this.estado.equals("Bahia")) {
            return estadosAbreviados.BA.toString();
        } else if (this.estado.equals("Ceará")) {
            return estadosAbreviados.CE.toString();
        } else if (this.estado.equals("Distrito Federal")) {
            return estadosAbreviados.DF.toString();
        } else if (this.estado.equals("Espírito Santo")) {
            return estadosAbreviados.ES.toString();
        } else if (this.estado.equals("Maranhão")) {
            return estadosAbreviados.GO.toString();
        } else if (this.estado.equals("Mato Grosso")) {
            return estadosAbreviados.MA.toString();
        } else if (this.estado.equals("Mato Grosso do Sul")) {
            return estadosAbreviados.MT.toString();
        } else if (this.estado.equals("Minas Gerais")) {
            return estadosAbreviados.MS.toString();
        } else if (this.estado.equals("Pará")) {
            return estadosAbreviados.MG.toString();
        } else if (this.estado.equals("Paraíba")) {
            return estadosAbreviados.PA.toString();
        } else if (this.estado.equals("Paraná")) {
            return estadosAbreviados.PB.toString();
        } else if (this.estado.equals("Pernambuco")) {
            return estadosAbreviados.PR.toString();
        } else if (this.estado.equals("Piauí")) {
            return estadosAbreviados.PE.toString();
        } else if (this.estado.equals("Goiás")) {
            return estadosAbreviados.PI.toString();
        } else if (this.estado.equals("Roraima")) {
            return estadosAbreviados.RR.toString();
        } else if (this.estado.equals("Rondônia")) {
            return estadosAbreviados.RO.toString();
        } else if (this.estado.equals("Rio de Janeiro")) {
            return estadosAbreviados.RJ.toString();
        } else if (this.estado.equals("Rio Grande do Norte")) {
            return estadosAbreviados.RN.toString();
        } else if (this.estado.equals("Rio Grande do Sul")) {
            return estadosAbreviados.RS.toString();
        } else if (this.estado.equals("Santa Catarina")) {
            return estadosAbreviados.SC.toString();
        } else if (this.estado.equals("São Paulo")) {
            return estadosAbreviados.SP.toString();
        } else if (this.estado.equals("Sergipe")) {
            return estadosAbreviados.SE.toString();
        } else if (this.estado.equals("Tocantins")) {
            return estadosAbreviados.TO.toString();
        } else {
            return "XX";
        }

    }

    private enum estadosAbreviados {
        AC, AL, AP, AM, BA, CE, DF, ES, GO, MA, MT, MS, MG, PA, PB, PR, PE, PI, RR, RO, RJ, RN, RS, SC, SP, SE, TO;
    }

}

